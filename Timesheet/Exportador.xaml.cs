﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Timesheet
{
    /// <summary>
    /// Interaction logic for Exportador.xaml
    /// </summary>
    public partial class Exportador : Window
    {
        public Exportador()
        {
            InitializeComponent();
            Data.DisplayModeChanged += Data_DisplayModeChanged;
        }

        private void Data_DisplayModeChanged(object sender, EventArgs e)
        {
            Data.DisplayModeChanged -= Data_DisplayModeChanged;

            Data.DisplayMode = CalendarMode.Year;

            string dia = "01";
            string year = Data.DisplayDate.Year.ToString();
            string month = Data.DisplayDate.Month.ToString();

            if (Data.DisplayDate.Month < 10)
                month = "0" + month;

            var StringData = string.Format("{0}/{1}/{2}", dia, month, year);

            Data.DisplayModeChanged += Data_DisplayModeChanged;

            Task.Run(() => Excel.CriarExcel(StringData));
        }


    }
}
